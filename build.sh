#!/usr/bin/env bash
cordova build --release android
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ~/.ssh/lupulocalizador.keystore platforms/android/build/outputs/apk/android-release-unsigned.apk Lupulocalizador
/usr/local/Cellar/android-sdk/24.1.2/build-tools/22.0.1/zipalign -v 4 platforms/android/build/outputs/apk/android-release-unsigned.apk ~/Desktop/Lupulocalizador-$1.apk
#scp ~/Desktop/Lupulocalizador-$1.apk root@104.236.44.184:/var/www/lupulocalizador