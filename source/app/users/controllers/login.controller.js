angular.module('Lupulocalizador')
    .controller('LoginController', ['$scope', '$ionicPopup', '$location', '$rootScope', '$window', '$ionicLoading', 'User', function ($scope, $ionicPopup, $location, $rootScope, $window, $ionicLoading, User) {
        $scope.user = {};
        $scope.errors = {};

        $scope.login = function(form){
            $scope.showLoader = true;
            $scope.submitted = true;

            if(form.$valid){

                var login = User.login($scope.user).then(function(response, code){
                    $ionicLoading.show({
                        template: '🔓<br />Logado',
                        duration: 2000
                    });
                    $scope.showLoader = false;
                    $scope.user = {};
                    $location.path("/app");
                }, function(){
                    $ionicPopup.alert({
                        title: 'Login',
                        template: 'Houve um erro ao efetuar o login. Verifique se o email e senha informados estão corretos ou se você está conectado com a internet.',
                        buttons: [
                            {
                                text: 'fechar',
                                type: 'btn-green'
                            }
                        ]
                    });
                    $scope.showLoader = false;
                });
            } else {
                $scope.showLoader = false;
            }
        };
    }]);
