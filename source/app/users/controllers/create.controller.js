angular.module('Lupulocalizador')
    .controller('CreateController', ['$scope', '$ionicPopup', '$state', '$rootScope', '$window', '$filter', 'User',
        function ($scope, $ionicPopup, $state, $rootScope, $window, $filter, User) {

        $scope.user = {};
        $scope.errors = {};
        var today = new Date();
        var age = new Date(today.getFullYear() - 18, today.getMonth(), today.getDate());

        $scope.create = function(form){
            $scope.showLoader = true;
            $scope.submitted = true;

            $scope.isAdult = ($scope.user.birthDate/1000 <= Math.round(age/1000));
            $scope.passwordMatch = ($scope.user.password === $scope.user.passwordConfirm);
            if(form.$valid && $scope.passwordMatch && $scope.isAdult){
                $scope.user.birth = $filter('date')($scope.user.birthDate, "yyyy-MM-dd");
                var created = User.create($scope.user).then(function(newUser){
                    var data = {
                        code: newUser.data.id,
                        name: $scope.user.name,
                        gender: $scope.user.gender,
                        birth: $scope.user.birth,
                        total_checkin: 0,
                        total_visited: 0,
                        total_created: 0,
                        email: [
                            {
                                email: $scope.user.email,
                                type: "Particular"
                            }
                        ],
                        phone: [
                            {
                                number: $scope.user.phone,
                                type: "Celular"
                            }
                        ]
                    };

                    $rootScope.$broadcast('user:updated', data);
                    User.user = data;
                    $window.localStorage.User = JSON.stringify(data);
                    $window.localStorage.accessToken = newUser.data['access-token'];

                    $ionicPopup.alert({
                        title: 'Cadastro',
                        template: 'O cadastro foi realizado com sucesso. Agora você pode aproveitar todos os recursos do Lupulocalizador.',
                        buttons: [
                            {
                                text: 'fechar',
                                type: 'btn-green',
                                onTap: function() {
                                    $scope.userCreated = true;
                                    $scope.showLoader = false;
                                }
                            }
                        ]
                    });
                }, function(message, code){
                    $ionicPopup.alert({
                        title: 'Cadastro',
                        template: 'Houve um erro ao efetuar o seu cadastro, pode ser que o email ou telefone informados já estejam cadastrados ou você está sem conexão com a internet.',
                        buttons: [
                            {
                                text: 'fechar',
                                type: 'btn-green'
                            }
                        ]
                    });
                    $scope.showLoader = false;
                });
            } else {
                $scope.showLoader = false;
            }
        };
    }]);
