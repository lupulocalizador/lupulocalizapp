angular.module('Lupulocalizador')
    .controller('UserController', ['$scope', 'User', function ($scope, User) {
        $scope.showLoader = true;
        User.getUpdatedUser().then(function(user){
            window.localStorage.User = JSON.stringify(user.data);
             $scope.profile = user.data;
            $scope.showLoader = false;
        }, function(){
            $scope.profile = JSON.parse(window.localStorage.User);
            $scope.showLoader = false;
        });
    }]);