angular.module('Lupulocalizador')
    .factory('User', ['$http', '$rootScope', '$window', '$q', 'Config', function($http, $rootScope, $window, $q, Config){
        var service = {};
        service.user = {};

        service.create = function(data){
            return $http.post(Config.endpoint + '/user', data);
        };

        service.getUser = function(){
            var logged = $window.localStorage.User;

            if(logged == null){
                return false;
            }
            return JSON.parse(logged);
        };

        service.getUpdatedUser = function(){
            var logged = $window.localStorage.User;

            if(logged != null && logged != undefined){
                logged = JSON.parse(logged);
                return $http.get(Config.endpoint + '/user/email:' + logged.email[0].email + '/phone:' + logged.phone[0].number);
            } else {
                return false;
            }
        };

        service.addCheckin = function(){
            var profile = JSON.parse($window.localStorage.getItem('User'));
            profile.total_checkin += 1;
            window.localStorage.User = JSON.stringify(profile);

            return true;
        };

        service.login = function(data){
            return $q(function(resolve, reject) {
                if($window.localStorage.accessToken == undefined){
                    $http.post(Config.endpoint + '/user/login/password', data).then(function(token, code){
                        $window.localStorage.accessToken = token.data['access-token'];

                        $http.get(Config.endpoint + '/user/email:' + data.email).then(function(user){
                            $rootScope.$broadcast('user:updated', user.data);
                            service.user = user.data;
                            $window.localStorage.User = JSON.stringify(user.data);

                            resolve(token.status);
                        }, function(){
                            reject(token.status);
                        });
                    }, function(token, code){
                        reject(code);
                    });
                } else {
                    reject(false);
                }
            });
        };

        service.logout = function(){
            $window.localStorage.removeItem('User');
            $window.localStorage.removeItem('accessToken');

            return true;
        };

        service.getToken = function(){
            return $window.localStorage.accessToken;
        };

        return service;
    }]);
