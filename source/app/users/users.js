angular.module('Lupulocalizador')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.user', {
                url: '/users/profile',
                views: {
                    'menuContent': {
                        templateUrl: 'app/users/templates/user.html',
                        controller: 'UserController'
                    }
                }
            })
            .state('app.create', {
                url: '/users/create',
                views: {
                    'menuContent': {
                        templateUrl: 'app/users/templates/create.html',
                        controller: 'CreateController'
                    }
                }
            })
            .state('app.login', {
                url: '/users/login',
                views: {
                    'menuContent': {
                        templateUrl: 'app/users/templates/login.html',
                        controller: 'LoginController'
                    }
                }
            });
    }]);