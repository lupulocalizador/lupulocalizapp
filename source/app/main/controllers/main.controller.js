angular.module('Lupulocalizador')
    .controller('MainController', [
        '$scope', '$location', '$state', '$cordovaGeolocation', '$stateParams', '$rootScope', '$ionicPopup', '$window', '$interval', 'leafletData', 'leafletEvents', 'Pin', 'backcallFactory',
        function ($scope, $location, $state, $cordovaGeolocation, $stateParams, $rootScope, $ionicPopup, $window, $interval, leafletData, leafletEvents, Pin, backcallFactory) {
            backcallFactory.backcallfun();

            var popupEstados;
            if($window.localStorage.lastLocation !== undefined){
                $scope.map = Pin.getMapDefaults(undefined, true);
                $scope.tiles = Pin.getTilesProvider();
            } else {
                popupEstados = $ionicPopup.show({
                    title: 'Escolha um Estado',
                    subtitle: 'Para começar',
                    templateUrl: 'app/main/templates/states-popup.html',
                    scope: $scope
                });
            }

            $scope.$on('$stateChangeSuccess', function() {
                // TODO: this is a hack...
                $interval(function() {
                    leafletData.getMap().then(function(map) {
                        map.invalidateSize();
                    });
                }, 500, 1);
                if($scope.showCard){
                    $scope.map.markers['B_' + $scope.card.id].icon = $scope.map.icons.default;
                }
                $scope.showCard = false;
            });

            $scope.$on('leafletDirectiveMap.moveend', function(event, args) {
                $scope.showLoader = true;
                leafletData.getMap().then(function (map) {
                    Pin.getPinsByBounds(map);
                    $scope.showLoader = false;
                });
            });

            $scope.$on("centerUrlHash", function(event, centerHash) {
                $location.search({ c: centerHash });
            });

            $rootScope.$on('state:changed', function(event,data) {
                $scope.map.center  = data;
            });

            $scope.$on("leafletDirectiveMap.click", function(ev, leafletPayload) {
                if($scope.showCard){
                    $scope.map.markers['B_' + $scope.card.id].icon = $scope.map.icons.default;
                    $scope.showCard = false;
                }
            });

            $scope.$on("leafletDirectiveMarker.click", function(ev, leafletPayload) {
                var pid = null;
                if($scope.card !== undefined){
                    pid = 'B_' + $scope.card.id;
                }

                if($scope.showCard && pid === leafletPayload.modelName){
                    $scope.map.markers[leafletPayload.modelName].icon = $scope.map.icons.default;
                    $scope.showCard = false;
                } else {

                    if(pid !== null){
                        $scope.map.markers[pid].icon = $scope.map.icons.default;
                    }

                    $scope.card = $scope.map.markers[leafletPayload.modelName].info;
                    $scope.map.center  = {lat: $scope.card.lat, lng: $scope.card.lng, zoom: $scope.map.center.zoom};
                    $scope.map.markers[leafletPayload.modelName].icon = $scope.map.icons.selected;
                    $scope.showCard = true;
                }
            });


            $scope.centerToStore = function(lat, lng){
                $scope.map.center  = {lat: lat, lng: lng, zoom: 16};
            };

            /**
             * Center map on user's current position
             */
            $scope.locate = function(){
                $scope.showLoader = true;
                $cordovaGeolocation
                    .getCurrentPosition()
                    .then(function (position) {
                        $scope.showCard = false;
                        $scope.map.center.lat  = position.coords.latitude;
                        $scope.map.center.lng = position.coords.longitude;
                        $scope.map.center.zoom = 15;

                        window.localStorage.lastLocation = JSON.stringify({lat: position.coords.latitude, lng: position.coords.longitude, zoom: 14});
                        $scope.showLoader = false;
                    }, function(err) {
                        // error
                        console.log("Location error!");
                        console.log(err);
                        $scope.showLoader = false;
                    });

            };

            $scope.changeState = function(index){
                var state = $scope.states[index];

                $scope.map = Pin.getMapDefaults(state, true);
                $scope.tiles = Pin.getTilesProvider();
                $scope.showCard = false;
                popupEstados.close();
            };

            $scope.states = [
                {name: 'Acre', city: 'Rio Branco', lat: -9.97472, lng: -67.81},
                {name: 'Alagoas', city: 'Maceió', lat: -9.66583, lng: -35.73528},
                {name: 'Amapá', city: 'Macapá', lat: 0.03889, lng: -51.06639},
                {name: 'Amazonas', city: 'Manaus', lat: -3.10194, lng: -60.025},
                {name: 'Bahia', city: 'Salvador', lat: -12.97111, lng: -38.51083},
                {name: 'Ceará', city: 'Fortaleza', lat: -3.71722, lng: -38.54306},
                {name: 'Distrito Federal', city: 'Brasília', lat: -15.77972, lng: -47.92972},
                {name: 'Espírito Santo', city: 'Vitória', lat: -20.31944, lng: -40.33778},
                {name: 'Goiás', city: 'Goiânia', lat: -16.67861, lng: -49.25389},
                {name: 'Maranhão', city: 'São Luiz', lat: -2.52972, lng: -44.30278},
                {name: 'Mato Grosso', city: 'Cuiabá', lat: -15.59611, lng: -56.09667},
                {name: 'Mato Grosso do Sul', city: 'Campo Grande', lat: -20.44278, lng: -54.64639},
                {name: 'Minas Gerais', city: 'Belo Horizonte', lat: -19.92083, lng: -43.93778},
                {name: 'Pará', city: 'Belém', lat: -1.45583, lng: -48.50444},
                {name: 'Paraiba', city: 'João Pessoa', lat: -7.115, lng: -34.86306},
                {name: 'Paraná', city: 'Curitiba', lat: -25.42778, lng: -49.27306},
                {name: 'Pernambuco', city: 'Recife', lat: -8.05389, lng: -34.88111},
                {name: 'Piauí', city: 'Teresina', lat: -5.08917, lng: -42.80194},
                {name: 'Rio de Janeiro', city: 'Rio de Janeiro', lat: -22.90278, lng: -43.2075},
                {name: 'Rio Grande do Norte', city: 'Natal', lat: -5.795, lng: -35.20944},
                {name: 'Rio Grande do Sul', city: 'Porto Alegre', lat: -30.03306, lng: -51.23},
                {name: 'Rondônia', city: 'Porto Velho', lat: -8.76194, lng: -63.90389},
                {name: 'Roraima', city: 'Boa Vista', lat: 2.81972, lng: -60.67333},
                {name: 'Santa Catarina', city: 'Florianópolis', lat: -27.59667, lng: -48.54917},
                {name: 'São Paulo', city: 'São Paulo', lat: -23.5475, lng: -46.63611},
                {name: 'Sergipe', city: 'Aracaju', lat: -10.91111, lng: -37.07167},
                {name: 'Tocantins', city: 'Palmas', lat: -10.16745, lng: -48.32766}
            ];
        }]);
