angular.module('Lupulocalizador')
    .controller('ContactController',  ['$scope', 'Contact', '$ionicPopup',function ($scope, Contact, $ionicPopup) {
        if($scope.User) {
            $scope.contact = {
                name: ($scope.User !== null) ? $scope.User.name : '',
                email: ($scope.User !== null) ? $scope.User.email[0].email : ''
            };
        } else {
            $scope.contact = {};
        }
        $scope.errors = {};

        $scope.sendContact = function(form){
            $scope.showLoader = true;
            $scope.submitted = true;

            if(form.$valid){

                Contact.sendContact($scope.contact).then(function(){
                    $ionicPopup.alert({
                        title: 'Contato',
                        template: 'Sua mensagem foi enviada com sucesso. Em breve a nossa equipe entrará em contato com você.',
                        buttons: [
                            {
                                text: 'fechar',
                                type: 'btn-green'
                            }
                        ]
                    });

                    if(typeof($scope.User) === Object){
                        $scope.contact = {
                            name: $scope.User.name,
                            email: $scope.User.email[0].email
                        };
                    } else {
                        $scope.contact = {
                            name: '',
                            email: ''
                        };
                    }
                    $scope.showLoader = false;
                    $scope.submitted = false;
                }, function(){
                    $ionicPopup.alert({
                        title: 'Contato',
                        template: 'Houve um erro ao enviar a sua mensagem, certifique-se que esteja conectado à internet e tente novamente.',
                        buttons: [
                            {
                                text: 'fechar',
                                type: 'btn-green'
                            }
                        ]
                    });
                    $scope.showLoader = false;
                });
            } else {
                $scope.showLoader = false;
            }
        };
    }]);
