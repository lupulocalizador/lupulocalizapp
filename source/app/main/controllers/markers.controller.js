angular.module('Lupulocalizador')
    .controller('MarkerController', ['$scope', '$interval', '$ionicPopup', 'Pin', 'leafletData', function ($scope, $interval, $ionicPopup, Pin, leafletData) {
        $scope.map = Pin.getMapDefaults();
        $scope.tiles = Pin.getTilesProvider();
        $scope.errors = {};
        $scope.store = {};

        $scope.saveStore = function(form){
            $scope.showLoader = true;
            $scope.submitted = true;

            if(form.$valid){
                $scope.store.lat = $scope.map.markers[0].lat;
                $scope.store.lng = $scope.map.markers[0].lng;
                Pin.createPin($scope.store).then(function(){
                    $scope.popup = $ionicPopup.alert({
                        title: 'Novo Estabelecimento',
                        template: 'O Estabelecimento foi salvo com sucesso! A sua sugestão está aguardando a aprovação da nossa equipe.',
                        buttons: [
                            {
                                text: 'fechar',
                                type: 'btn-green'
                            }
                        ]
                    });
                }, function(){
                    $scope.popup = $ionicPopup.alert({
                        title: 'Novo Estabelecimento - Erro',
                        template: 'Houve um erro ao salvar o estabelecimento',
                        buttons: [
                            {
                                text: 'fechar',
                                type: 'btn-green'
                            }
                        ]
                    });
                });
            } else {
                $scope.popup = $ionicPopup.alert({
                    title: 'Novo Estabelecimento',
                    template: 'Parece que você não preencheu todos os campos obrigatórios',
                    buttons: [
                        {
                            text: 'fechar',
                            type: 'btn-green'
                        }
                    ]
                });
            }
        };

        $scope.$on('$stateChangeSuccess', function() {
            // TODO: this is a hack...
            $interval(function() {
                leafletData.getMap().then(function(map) {
                    map.invalidateSize();
                });
            }, 500, 1);

            $scope.$on("leafletDirectiveMap.click", function(event, args){
                var leafEvent = args.leafletEvent;
                $scope.map.markers = [];
                $scope.map.markers.push({
                    lat: leafEvent.latlng.lat,
                    lng: leafEvent.latlng.lng,
                    focus: true,
                    message: "Arraste para mudar a posição do Pin",
                    draggable: true,
                    icon: Pin.icons.default
                });
                $scope.map.center = {lat: leafEvent.latlng.lat, lng: leafEvent.latlng.lng, zoom: 16};
                $scope.map.height = 200;

                $scope.showForm = true;
            });
        });
    }]);
