angular.module('Lupulocalizador')
    .directive('loader', function(){
        return {
            restrict: 'EA',
            replace: 'true',
            templateUrl: 'app/main/templates/loader.html'
        };
    });