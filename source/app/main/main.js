angular.module('Lupulocalizador')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.main', {
                url: '/maps/:latlng',
                views: {
                    'menuContent': {
                        templateUrl: 'app/main/templates/main.html',
                        controller: 'MainController'
                    }
                }
            })
            .state('app.states', {
                url: '/states',
                views: {
                    'menuContent': {
                        templateUrl: 'app/main/templates/states.html',
                        controller: 'StatesController'
                    }
                }
            }).state('app.about', {
                url: '/about',
                views: {
                    'menuContent': {
                        templateUrl: 'app/main/templates/about.html',
                        controller: 'AboutController'
                    }
                }
            }).state('app.contact', {
                url: '/contact',
                views: {
                    'menuContent': {
                        templateUrl: 'app/main/templates/contact.html',
                        controller: 'ContactController'
                    }
                }
            }).state('app.addMarker', {
                url: '/marker',
                views: {
                    'menuContent': {
                        templateUrl: 'app/main/templates/marker.html',
                        controller: 'MarkerController'
                    }
                },
                onEnter: ['$ionicPopup', '$state', 'User', function($ionicPopup, $state, User){
                    if(!User.getUser()){
                        $ionicPopup.alert({
                            title: 'Novo Estabelecimento',
                            template: 'É necessário estar logado para sugerir estabelecimentos.',
                            buttons: [
                                {
                                    text: 'fechar',
                                    type: 'btn-green'
                                }
                            ]
                        });

                        $state.go('app.login');
                    };
                }],
            });
    }]);
