angular.module('Lupulocalizador')
    .factory('Contact', ['$http', 'Config', function($http, Config){
        var service = Object.create;

        service.sendContact = function(data){
            data.message += "\n\n Enviado via " + ionic.Platform.platform() + " - Versão " + ionic.Platform.version();
            return $http.post(Config.endpoint + '/contact', data);
        };

        return service;
    }]);
