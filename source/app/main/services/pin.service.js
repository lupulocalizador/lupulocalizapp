angular.module('Lupulocalizador')
    .factory('Pin', ['$http', '$q', '$window', 'Config', 'User', function($http, $q, $window, Config, User){
        var service = Object.create;
        var canceler;
        service.currentLocation = $window.localStorage.getItem('lastLocation');
        service.pins = {};
        service.icons = {
            default: {
                iconUrl: 'images/icon.png',
                    iconSize:     [30, 43], // size of the icon
                    iconAnchor:   [16, 42], // point of the icon which will correspond to marker's location
                    popupAnchor:  [0, -42] // point from which the popup should open relative to the iconAnchor
            },
            selected: {
                iconUrl: 'images/icon-sl.png',
                    iconSize:     [30, 43], // size of the icon
                    iconAnchor:   [16, 42], // point of the icon which will correspond to marker's location
                    popupAnchor:  [0, -42] // point from which the popup should open relative to the iconAnchor
            }
        };

        /**
         * Returns the default map data
         *
         * @returns {{defaults: {tileLayer: string, maxZoom: number, zoomControlPosition: string}, center: *, markers: *, tileLayerOptions: {attribution: null}, events: {map: {enable: string[], logic: string}}}}
         */
        service.getMapDefaults = function(location, loadPins){
            return {
                icons: service.icons,
                defaults: {
                    tileLayer: 'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
                    maxZoom: 18,
                    zoomControlPosition: 'bottomleft',
                    scrollWheelZoom: false
                },
                center : (service.currentLocation != undefined) ? JSON.parse(service.currentLocation) : {lat: location.lat, lng: location.lng, zoom: 14},
                markers : loadPins ? service.pins : {},
                tileLayerOptions: {
                    attribution: null
                },
                events: {
                    map: {
                        enable: ['context', 'click'],
                        logic: 'emit'
                    }
                },
                tiles: service.getTilesProvider('osm'),
                layers: {
                    baselayers: {
                        osm: {
                            name: 'Open Street Maps',
                            url: 'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
                            type: 'xyz'
                        },
                        mapbox: {
                            name: 'Mapbox',
                            url: 'http://api.tiles.mapbox.com/v4/{mapid}/{z}/{x}/{y}.png?access_token={apikey}',
                            type: 'xyz',
                            layerParams: {
                                apikey: 'pk.eyJ1IjoibHVwdWxvY2FsaXphZG9yIiwiYSI6IjczZWIyMTU5OWYxNjIxMDZiYzY1YmM4MWQ2YTM2NWE3In0.FxzKMIL6L8LGXVIc0WQlXA',
                                mapid: 'lupulocalizador.n0k4peep'
                            }
                        }
                    }
                }
            };
        };

        /**
         * Get the main map tiles provider
         * @param provider
         * @returns {*}
         */
        service.getTilesProvider = function(provider){
            var providers = {
                osm: {
                    name: 'Open Street Maps',
                    url: 'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
                    type: 'xyz'
                },
                mapbox: {
                    name: 'Mapbox',
                    url: 'http://api.tiles.mapbox.com/v4/{mapid}/{z}/{x}/{y}.png?access_token={apikey}',
                    type: 'xyz',
                    options: {
                        apikey: 'pk.eyJ1IjoibHVwdWxvY2FsaXphZG9yIiwiYSI6IjczZWIyMTU5OWYxNjIxMDZiYzY1YmM4MWQ2YTM2NWE3In0.FxzKMIL6L8LGXVIc0WQlXA',
                        mapid: 'lupulocalizador.n0k4peep'
                    }
                }
            };

            return providers[provider];
        };

        service.saveLocation = function(map){
            return $window.localStorage.lastLocation = JSON.stringify({lat: map.getCenter().lat, lng: map.getCenter().lng, zoom: 14});
        };

        /**
         * Get pins based in actual map bounds
         * @param map
         */
        service.getPinsByBounds = function(map){
            var bounds = map.getBounds();

            service.saveLocation(map);

            var northEast = bounds.getNorthEast().toString().replace('LatLng(', '').replace(')', '').split(', ');
            var southWest = bounds.getSouthWest().toString().replace('LatLng(', '').replace(')', '').split(', ');

            if(canceler) canceler.resolve();
            canceler = $q.defer();

            $http.get(Config.endpoint + '/pins?' +
                'minLat=' + southWest[0] + new String('00') +
                '&minLng=' + southWest[1] + new String('00') +
                '&maxLat=' + northEast[0] + new String('00') +
                '&maxLng=' + northEast[1] + new String('00')
            ).then(function(pins){
                angular.forEach(pins.data, function(value, key){

                    if(typeof value === 'object' && value !== null && service.pins[key] === undefined){

                        service.pins[key] = {
                            //group: value.district,
                            draggable: false,
                            focus: false,
                            lat: value.lat,
                            lng: value.lng,
                            //message: '<div class="message"><h4>' + value.name + '</h4><br /><a href="#/app/stores/' + key + '">mais informações</a></div>',
                            info: value,
                            pin: key,
                            icon: service.icons.default,
                            label: {
                                message: value.name,
                                options: {
                                    noHide: true
                                }
                            }
                        };
                        //Set each pin to localStorage
                        //window.localStorage.setItem('markers', JSON.stringify(service.pins));
                    }
                });
            });
        };

        service.createPin = function(pin){
            return $q(function(resolve, reject) {
                pin.phones = [{
                    number: pin.phone,
                    id_phone_type: 1
                }];

                delete pin.phone;
                pin['access-token'] = User.getToken();

                $http.post(Config.endpoint + '/pin', pin).then(function(response){
                    resolve(response.data);
                }, function(code, message){
                    reject(false);
                });
            });
        };

        return service;
    }]);
