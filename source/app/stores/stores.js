angular.module('Lupulocalizador')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.stores', {
                url: '/stores/:id',
                views: {
                    'menuContent': {
                        templateUrl: 'app/stores/templates/stores.html',
                        controller: 'StoresController'
                    }
                }
            })
    }]);