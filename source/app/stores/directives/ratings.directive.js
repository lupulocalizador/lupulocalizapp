angular.module('Lupulocalizador')
    .directive('storeRatings', function(){
        return {
            restrict: 'EA',
            replace: 'true',
            templateUrl: 'app/stores/templates/store-ratings.html'
        };
    });