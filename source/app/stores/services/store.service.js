angular.module('Lupulocalizador')
    .factory('Store', ['$http', 'Config', 'User', function($http, Config, User){
        var service = Object.create;
        service.store = Object.create;

        /**
         * Get a single pin
         * @param pin
         * @returns object
         */
        service.getPin = function(id){
            return $http.get(Config.endpoint + '/pin/' + id);
        };

        /**
         *
         * @param storeId
         * @returns {*}
         */
        service.setCheckin = function(storeId){
            if(!User.getUser()){
                return false;
            }

            return $http.post(Config.endpoint + '/pin/checkin/' + storeId, {
                'access-token': User.getToken()
            });
        };

        /**
         *
         * @param storeId
         * @param rankings
         * @returns {*}
         */
        service.setRanking = function(storeId, rankings){
            if(!User.getUser()){
                return false;
            }

            return $http.post(Config.endpoint + '/pin/ranking/' + storeId, {
                'access-token': User.getToken(),
                ranking: rankings
            });
        };

        return service;
    }]);