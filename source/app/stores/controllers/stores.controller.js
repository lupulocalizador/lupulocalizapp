angular.module('Lupulocalizador')
    .controller('StoresController', ['$scope', '$stateParams', '$ionicPopup', '$ionicModal', 'Store', 'User', function ($scope, $stateParams, $ionicPopup, $ionicModal, Store, User) {
        $scope.rating = {
            variety_food: 0,
            variety_beer: 0,
            price: 0,
            service: 0,
            ambience: 0
        };

        $scope.showLoader = true;
        Store.getPin($stateParams.id).then(function(pin){
            $scope.storeGeneralRanking = pin.data.ranking.general;
            delete pin.data.ranking.general;
            Store.store = pin.data;
            $scope.store = pin.data;
            $scope.showLoader = false;
        }, function(){
            $scope.showLoader = false;
            return false;
        });

        $scope.openMap = function(){
            var address = encodeURI($scope.store.address + ', ' + $scope.store.district + ' - ' + $scope.store.city) + ' - ' + $scope.store.state;
            var lat = parseFloat($scope.store.lat);
            var long = parseFloat($scope.store.lng);

            if(ionic.Platform.isIOS()) {
                window.open("http://maps.apple.com/?q=" + address + "&ll=" + lat + "," + long + "&near=" + lat + "," + long, '_system', 'location=yes');
            } else {
                window.open("geo:" + lat + "," + long + "?q=" + address, '_system', 'location=yes');
            }
        };

        $scope.openLink = function(link){
            window.open(link, '_system');
            return true;
        };

        $scope.checkin = function(){
            $scope.showLoader = true;
            if(User.getUser()){
                Store.setCheckin($scope.store.id).success(function(data, status){
                    User.addCheckin(); // save checkin on local profile

                    $scope.popup = $ionicPopup.alert({
                        title: 'Checkin',
                        template: 'Pronto, você marcou que está em ' + $scope.store.name,
                        buttons: [
                            {
                                text: 'fechar',
                                type: 'btn-green'
                            }
                        ]
                    });
                    $scope.showLoader = false;
                }).error(function(data, status){
                    console.log($scope.popup);
                    if($scope.popup !== undefined){
                        $scope.popup.close();
                    }

                    if(status == 403){
                        $scope.popup = $ionicPopup.alert({
                            title: 'Checkin',
                            template: 'Ops, parece que você já fez checkin hoje em ' + $scope.store.name,
                            buttons: [
                                {
                                    text: 'fechar',
                                    type: 'btn-green'
                                }
                            ]
                        });
                    } else {
                        $scope.popup = $ionicPopup.alert({
                            title: 'Checkin',
                            template: 'Houve um erro ao fazer seu checkin em ' + $scope.store.name,
                            buttons: [
                                {
                                    text: 'fechar',
                                    type: 'btn-green'
                                }
                            ]
                        });
                    }
                    $scope.showLoader = false;
                });
            } else {
                $scope.popup = $ionicPopup.alert({
                    title: 'Checkin',
                    template: 'É necessário estar logado para fazer checkin',
                    buttons: [
                        {
                            text: 'fechar',
                            type: 'btn-green'
                        }
                    ]
                });
                $scope.showLoader = false;
            }
        };

        $scope.openRatingsModal = function(){
            if (User.getUser()) {
                $ionicModal.fromTemplateUrl('app/stores/templates/ratings.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function(modal) {
                    $scope.ratingsModal = modal;
                    modal.show();
                });
            } else {
                $ionicPopup.alert({
                    title: 'Checkin',
                    template: 'É necessário estar logado para fazer avaliações.',
                    buttons: [
                        {
                            text: 'fechar',
                            type: 'btn-green'
                        }
                    ]
                });
                $scope.showLoader = false;
            }
        };


        $scope.resetRanking = function(ranking){
            $scope.rating[ranking] = 0;
        };

        $scope.saveRanking = function() {
            $scope.showLoader = true;
            var ratings = [];
            angular.forEach($scope.rating, function (value, key) {
                ratings.push({
                    code: key,
                    ranking: value
                });
            });

            if (User.getUser()) {
                Store.setRanking($scope.store.id, ratings).success(function (data, status) {
                    // Get the updated Pin
                    Store.getPin($stateParams.id).then(function(pin){
                        $scope.storeGeneralRanking = pin.data.ranking.general;
                        delete pin.data.ranking.general;
                        Store.store = pin.data;
                        $scope.store = pin.data;

                        $scope.rating = {
                            variety_food: 0,
                            variety_beer: 0,
                            price: 0,
                            service: 0,
                            ambience: 0
                        };
                        $scope.ratingsModal.hide();
                        $scope.showLoader = false;
                    }, function(){
                        $scope.showLoader = false;
                        return false;
                    });

                    $ionicPopup.alert({
                        title: 'Avaliação',
                        template: 'Pronto, você acabou de avaliar ' + $scope.store.name,
                        buttons: [
                            {
                                text: 'fechar',
                                type: 'btn-green'
                            }
                        ]
                    });

                }).error(function (data, status) {
                    if (status == 403) {
                        $ionicPopup.alert({
                            title: 'Checkin',
                            template: 'Parece que você já avaliou ' + $scope.store.name + ' antes.',
                            buttons: [
                                {
                                    text: 'fechar',
                                    type: 'btn-green'
                                }
                            ]
                        });

                    } else {
                        $ionicPopup.alert({
                            title: 'Checkin',
                            template: 'Houve um erro ao salvar sua avaliação em ' + $scope.store.name,
                            buttons: [
                                {
                                    text: 'fechar',
                                    type: 'btn-green'
                                }
                            ]
                        });
                    }
                    $scope.showLoader = false;
                });
            } else {
                $ionicPopup.alert({
                    title: 'Checkin',
                    template: 'É necessário estar cadastrado para fazer avaliações.',
                    buttons: [
                        {
                            text: 'fechar',
                            type: 'btn-green'
                        }
                    ]
                });
                $scope.showLoader = false;
            }
        };
    }]);