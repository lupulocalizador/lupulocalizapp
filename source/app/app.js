'use strict';

/**
 * cordova#4.3.1
 * @todo Adicionar o Google maps (x)
 * @todo Criar opção no perfil para alterar o serviço de mapas (-)
 * @todo Melhorias na splash screen
 * @todo Voltar a utilizar o ícone antigo
 * @todo Exibir pin clicado, alterando a cor do pin
 */

angular.module('Lupulocalizador', [
    'ionic',
    'ngCordova',
    'leaflet-directive',
    'ui.utils',
    'ionic.rating',
    'ionic-datepicker',
    'Lupulocalizador.config'
])

    .run(['$ionicPlatform', function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    }])

    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $httpProvider) {
        $httpProvider.defaults.headers.post = {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        };

        $httpProvider.defaults.headers.put = {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        };

        $httpProvider.defaults.headers.delete = {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        };

        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'menu.html',
                controller: 'AppController'
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/maps/');

    }])
    .factory('Online', ['$http', function($http){
        return $http.get('http://jsonip.appspot.com/').then(function(data){
            window.localStorage.lastIp = data.ip;
            return data.ip;
        }, function(){
            return false;
        });
    }])
    .controller('AppController', ['$scope', '$state', '$ionicLoading', 'User', function($scope, $state, $ionicLoading, User) {
        $scope.$on('user:updated', function(event,data) {
            // you could inspect the data to see if what you care about changed, or just update your own scope
            $scope.User = data;
        });

        $scope.logout = function(){
            User.logout();
            $scope.User = undefined;
            $ionicLoading.show({
                template: '🔒<br />Deslogado',
                duration: 2000
            });
            $state.go('app.main');
        };

        $scope.User = User.getUser();
    }])
    .factory('backcallFactory', ['$state', '$ionicPlatform', '$ionicHistory', '$timeout', '$ionicPopup', function($state, $ionicPlatform, $ionicHistory, $timeout, $ionicPopup){
        var service = {};
        service.backcallfun = function(){
            $ionicPlatform.registerBackButtonAction(function(){
                if($state.current.name === 'app.main'){

                    $ionicPopup.confirm({
                        title: 'Sair da Aplicação',
                        template: 'Deseja fechar o Lupulocalizador?',
                        buttons: [{
                            text: 'Não',
                            type: 'button-default',
                            onTap: function(e) {

                            }
                        }, {
                            text: 'Sim',
                            type: 'btn-green',
                            onTap: function(e) {
                                // Returning a value will cause the promise to resolve with the given value.
                                return true;
                            }
                        }]
                    })
                    .then(function(res) {
                        if(res) {
                            navigator.app.exitApp();
                        }
                    });
                } else {
                    $ionicHistory.nextViewOptions({
                        disableBack: true
                    });

                    $state.go('app.main');
                }
            }, 100);
        };

        return service;
    }])
;